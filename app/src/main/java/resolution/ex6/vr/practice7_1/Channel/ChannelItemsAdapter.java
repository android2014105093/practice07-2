package resolution.ex6.vr.practice7_1.Channel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import resolution.ex6.vr.practice7_1.R;

/**
 * Created by SSELAB on 2016-10-11.
 */

public class ChannelItemsAdapter extends ArrayAdapter<ChannelItem> {

    Context context = null;
    int layoutResourceID = 0;
    ChannelItem[] channelItems = null;

    public ChannelItemsAdapter(Context context, int resource, ChannelItem[] channelItems) {
        super(context, resource, channelItems);

        this.context = context;
        this.layoutResourceID = resource;
        this.channelItems = channelItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutChannelItem = null;

        if (null != convertView) {
            layoutChannelItem = (LinearLayout)convertView;
        }
        else {
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutChannelItem = (LinearLayout)layoutInflater.inflate(layoutResourceID, null);
        }

        ImageView image = (ImageView)layoutChannelItem.findViewById(R.id.imageView2);
        TextView description = (TextView)layoutChannelItem.findViewById(R.id.descriptionText);
        TextView src = (TextView)layoutChannelItem.findViewById(R.id.srcText);
        ChannelItem currentChannelItem = channelItems[position];

        image.setImageResource(channelItems[position].imageID);
        description.setText(currentChannelItem.descriptionID);
        src.setText(currentChannelItem.srcID);

        return (View) layoutChannelItem;
    }
}
