package resolution.ex6.vr.practice7_1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import resolution.ex6.vr.practice7_1.Channel.ChannelItem;
import resolution.ex6.vr.practice7_1.Channel.ChannelItemsAdapter;


public class ChannelFragment extends Fragment {


    public ChannelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_channel, container, false);

        ChannelItem[] channelItems = new ChannelItem[5];
        for (int i = 0; i < 5; i++) {
            channelItems[i] = new ChannelItem();

            channelItems[i].imageID = R.drawable.test_channel;
            channelItems[i].descriptionID = R.string.test;
            channelItems[i].srcID = R.string.test;
        }

        ChannelItemsAdapter adpt = new ChannelItemsAdapter(this.getActivity(), R.layout.channel, channelItems);
        GridView gv = (GridView) view.findViewById(R.id.channelGrid);

        gv.setAdapter(adpt);

        return view;
    }
}
